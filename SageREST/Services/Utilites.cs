﻿using SageREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SageREST.Services
{
    public class Utilites
    {
        public SageOrderMsg BuildOrderMsg (string value)
        {
            // build a response order message from the stored procedure message return
            if (string.IsNullOrEmpty(value))
                return null;

            SageOrderMsg OrderMsg = new SageOrderMsg();
            string[] commasep;

            if (value == "Success")
            {
                OrderMsg.SuccessFail = value;
            }
            else
            {
                commasep = value.Split(',');

                foreach (string commaval in commasep)
                {
                    string[] pipesep;
                    pipesep = commaval.Split('|');

                    switch (pipesep[0].ToString())
                    {
                        case "Fail":
                            OrderMsg.SuccessFail = pipesep[0].ToString();
                            break;

                        case "Customer":
                            OrderMsg.Customer = pipesep[1].ToString();
                            OrderMsg.CustomerValid = pipesep[2].ToString();
                            break;

                        case "WebOrderNo":
                            OrderMsg.WebOrderNo = pipesep[1].ToString();
                            OrderMsg.WebOrderNoValid = pipesep[2].ToString();
                            break;

                        case "Product":
                            OrderMsg.Product = pipesep[1].ToString();
                            OrderMsg.ProductValid = pipesep[2].ToString();
                            break;

                        case "LineNumber":
                            OrderMsg.LineNumber = pipesep[1].ToString();
                            OrderMsg.LineNumberValid = pipesep[2].ToString();
                            break;

                        default:
                            break;
                    }

                }
            }

            return OrderMsg;
        }
    }
}