﻿using SageREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace SageREST.Services
{
    public class SageRepository : IDisposable
    {
        private SageDBDataContext dataContext = new SageDBDataContext();



        public string GetServiceCheck()
        {
            // get database online check
            try
            {
                ine_web_dbstatusResult dbstatus = dataContext.ine_web_dbstatus().SingleOrDefault();
                return dbstatus.DBStatus.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ine_web_customer_listResult> GetCustomerList()
        {
            Utilites utils = new Utilites();
            SageOrderMsg OrderMsg = new SageOrderMsg();
            OrderMsg = utils.BuildOrderMsg("");

            // get list of customers
            try
            {
                List<ine_web_customer_listResult> CustomerList = dataContext.ine_web_customer_list().ToList<ine_web_customer_listResult>();
                return CustomerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ine_web_product_listResult> GetProductList(string CustomerIn)
        {
            // get list of products for a specific customer
            try
            {
                string Customer;
                if (String.IsNullOrEmpty(CustomerIn))
                {
                    Customer = null;
                }
                else
                {
                    Customer = CustomerIn;
                }

                List<ine_web_product_listResult> ProductList = dataContext.ine_web_product_list(Customer).ToList<ine_web_product_listResult>();
                return ProductList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ine_web_product_qty_priceResult> GetProductStatusList(string CustomerIn, string ProductIn, string QtyIn)
        {
            // get product status details for a specific customer, product, and quantity
            try
            {
                double Qty = Convert.ToDouble(QtyIn);
                List <ine_web_product_qty_priceResult> ProductStatus = dataContext.ine_web_product_qty_price(CustomerIn, ProductIn, Qty).ToList<ine_web_product_qty_priceResult>();
                return ProductStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ine_web_order_statusResult> GetWebOrderStatusList(string CustomerIn)
        {
            // get list of web order status for a specific customer
            try
            {
                string Customer;
                if (String.IsNullOrEmpty(CustomerIn))
                {
                    Customer = null;
                }
                else
                {
                    Customer = CustomerIn;
                }

                List<ine_web_order_statusResult> WebOrderStatusList = dataContext.ine_web_order_status(Customer).ToList<ine_web_order_statusResult>();
                return WebOrderStatusList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ine_web_order_status_by_WebOrdResult> GetWebOrder(string WebOrderIn)
        {
            // get details of a specific web order
            try
            {
                string WebOrder;
                if (String.IsNullOrEmpty(WebOrderIn))
                {
                    WebOrder = null;
                }
                else
                {
                    WebOrder = WebOrderIn;
                }

                List<ine_web_order_status_by_WebOrdResult> WebOrderList = dataContext.ine_web_order_status_by_WebOrd(WebOrder).ToList<ine_web_order_status_by_WebOrdResult>();
                return WebOrderList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SageOrderReturn PostOrder(SageOrder OrderIn)
        {
            Utilites utils = new Utilites();

            SageOrderReturn OrderReturnList = new SageOrderReturn();

            // post sage order for processing, looping through each input line
            // on error fail and return failure status
            try
            {
                OrderReturnList.Customer = OrderIn.Customer;
                OrderReturnList.WebOrderNo = OrderIn.WebOrderNo;
                OrderReturnList.Email = OrderIn.Email;
                OrderReturnList.Name = OrderIn.Name;
                OrderReturnList.Address1 = OrderIn.Address1;
                OrderReturnList.Address2 = OrderIn.Address2;
                OrderReturnList.Locality = OrderIn.Locality;
                OrderReturnList.State = OrderIn.State;
                OrderReturnList.PostCode = OrderIn.PostCode;
                OrderReturnList.DateRequired = OrderIn.DateRequired;
                List<SageOrderLinesReturn> LineReturnList = new List<SageOrderLinesReturn>();

                foreach (SageOrderLines SOLine in OrderIn.OrderLines)
                {
                    SageOrderMsg OrderMsg = new SageOrderMsg();
                    SageOrderLinesReturn LineReturn = new SageOrderLinesReturn();

                    ine_web_order_createResult OrderReturnLineReturn = dataContext.ine_web_order_create(OrderIn.Customer, OrderIn.WebOrderNo, OrderIn.Email, OrderIn.Name, OrderIn.Address1, OrderIn.Address2, OrderIn.Locality,
                        OrderIn.State, OrderIn.PostCode, OrderIn.DateRequired, SOLine.LineNumber, SOLine.Product, SOLine.Qty, SOLine.PriceExGST, SOLine.LineTotal).SingleOrDefault<ine_web_order_createResult>();

                    OrderMsg = utils.BuildOrderMsg(OrderReturnLineReturn.Return.ToString());

                    OrderReturnList.OrderNo = OrderReturnLineReturn.OrderNo;
                    LineReturn.LineNumber = Convert.ToInt32(OrderReturnLineReturn.LineNumber);
                    LineReturn.Product = OrderReturnLineReturn.Product;
                    LineReturn.Qty = Convert.ToDouble(OrderReturnLineReturn.Qty);
                    LineReturn.PriceExGST = Convert.ToDouble(OrderReturnLineReturn.PriceExGST);
                    LineReturn.LineTotal = Convert.ToInt32(OrderReturnLineReturn.LineTotal);
                    LineReturn.OrderMsg = OrderMsg;

                    if (OrderMsg.SuccessFail == "Fail")
                    {
                        LineReturnList.Add(LineReturn);
                        OrderReturnList.OrderLines = LineReturnList;
                        break;
                    }
                    else
                    {
                        LineReturnList.Add(LineReturn);
                        OrderReturnList.OrderLines = LineReturnList;
                    }
                }

                return OrderReturnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            if (dataContext != null)
            {
                dataContext.Dispose();
            }
        }
    }
}