﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SageREST.Models
{
    public class SageOrder
    {
        public string Customer { get; set; }
        public string WebOrderNo { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public DateTime DateRequired { get; set; }
        public List<SageOrderLines> OrderLines { get; set; }
    }

    public class SageOrderLines
    {
        public int LineNumber { get; set; }
        public string Product { get; set; }
        public double Qty { get; set; }
        public double PriceExGST { get; set; }
        public int LineTotal { get; set; }
    }

    public class SageOrderReturn
    {
        public string Customer { get; set; }
        public string WebOrderNo { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public DateTime DateRequired { get; set; }
        public string OrderNo { get; set; }
        public List<SageOrderLinesReturn> OrderLines { get; set; }
    }

    public class SageOrderLinesReturn
    {
        public int LineNumber { get; set; }
        public string Product { get; set; }
        public double Qty { get; set; }
        public double PriceExGST { get; set; }
        public int LineTotal { get; set; }
        public SageOrderMsg OrderMsg { get; set; }
    }

    public class SageOrderMsg
    {
        public string SuccessFail { get; set; }
        public string Customer { get; set; }
        public string CustomerValid { get; set; }
        public string WebOrderNo { get; set; }
        public string WebOrderNoValid { get; set; }
        public string Product { get; set; }
        public string ProductValid { get; set; }
        public string LineNumber { get; set; }
        public string LineNumberValid { get; set; }
    }
}