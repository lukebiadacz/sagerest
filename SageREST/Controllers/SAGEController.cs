﻿using SageREST.Services;
using SageREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SageREST.Controllers
{
    public class SAGEController : ApiController
    {
        private SageRepository SageRepository;

        public SAGEController()
        {
            this.SageRepository = new SageRepository();
        }

        // GET: api/SAGEController/ServiceCheck
        [HttpGet]
        [Route("api/SAGEController/ServiceCheck")]
        public HttpResponseMessage ServiceCheck()
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetServiceCheck());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }


        // GET: api/SAGEController/SageCustomerList
        [HttpGet]
        [Route("api/SAGEController/SageCustomerList")]
        public HttpResponseMessage SageCustomerList()
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetCustomerList());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

        // GET: api/SAGEController/SageProductList?Customer=ABC
        [HttpGet]
        [Route("api/SAGEController/SageProductList")]
        public HttpResponseMessage SageProductList([FromUri] string Customer = "")
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetProductList(Customer));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

        // GET: api/SAGEController/SageProductStatusList?Customer=ABC&Product=DEF&Qty=1
        [HttpGet]
        [Route("api/SAGEController/SageProductStatusList")]
        public HttpResponseMessage SageProductStatusList([FromUri] string Customer = "", [FromUri] string Product = "", [FromUri] string Qty = "")
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetProductStatusList(Customer, Product, Qty));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

        // GET: api/SAGEController/SageWebOrderStatusList?Customer=ABC
        [HttpGet]
        [Route("api/SAGEController/SageWebOrderStatusList")]
        public HttpResponseMessage SageWebOrderStatusList([FromUri] string Customer = "")
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetWebOrderStatusList(Customer));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

        // POST: api/SAGEController/SageWebOrder
        [HttpPost]
        [Route("api/SAGEController/SageWebOrder")]
        public HttpResponseMessage SageWebOrder([FromBody]SageOrder Order)
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.PostOrder(Order));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

        // GET: api/SAGEController/SageWebOrder?Order=000001
        [HttpGet]
        [Route("api/SAGEController/SageWebOrder")]
        public HttpResponseMessage SageWebOrder([FromUri] string Order = "")
        {
            HttpResponseMessage response;
            try
            {
                response = Request.CreateResponse(HttpStatusCode.OK, this.SageRepository.GetWebOrder(Order));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            finally
            {
                this.SageRepository.Dispose();
            }

            return response;
        }

    }
}
